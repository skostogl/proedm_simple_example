import matplotlib.pyplot as plt
# pip install xsuite
import xtrack as xt
# pip install cpymad
from cpymad.madx import Madx
import xtrack as xt
import xpart as xp
import sys
sys.path.append('.')
from ibs_lib.IBSfunctions import *
import pandas as pd

madx = Madx()

mp      = 938.272046 #MeV/c^2
def gammarel(EGeV,m0=mp):
  return (EGeV*1.e3)/m0

def betarel(EGeV,m0=mp):
  g=gammarel(EGeV,m0=m0)
  return np.sqrt(1-1/g**2)

def cmp_frev(EGeV, C, m0=mp):
  b = betarel(EGeV)
  return (b*scipy.constants.speed_of_light/C)

def eta(gtrans,EGeV,m0=mp):
  g=gammarel(EGeV,m0=m0)
  eta = 1./g**2-1./gtrans**2
  return eta

CC_MV           = 4.0 
CC_LAG          = 0 # below transition
CC_H            = 6
bunch_intensity = 1e9
energy_GEV      = 0.968

emit_x          = 1e-6*betarel(energy_GEV)*gammarel(energy_GEV)  # normalised emit
emit_y          = 1e-6*betarel(energy_GEV)*gammarel(energy_GEV) # normalised emit
sigma_z         = 692e-2
sigma_ns        = sigma_z/scipy.constants.speed_of_light*4.

C               = 123.778
frev            = cmp_frev(energy_GEV, C) # in Hz
dt              = 10./frev # time step (s) for IBS computation, every 5 minutes
duration        = 500./frev # total duration (s)


madx.input(f'''
option,-echo,threader;

	QDk=-0.3;
	Qfk=0.05;


	N.Ebend=16;
	l.Ebend=4.809875;
	l.Quad=0.200;
	
	angle=2*pi/(N.Ebend);
	
	k.Qf:=Qfk;
	k.Qd:=QDk;
	k.Qss:=Qss;
	
	Qf: QUADRUPOLE, L= l.Quad, K1=k.Qf;
	dq:  drift, l = 0.2;
	ds:  drift, l = 1.6;
	BPM: monitor, l=0.08;
	dEbend: drift, l=0.42;
	d: Marker;
	SSQ:  QUADRUPOLE, L= l.Quad, K1=k.Qss;
	Qd: QUADRUPOLE, L= l.Quad, K1=k.Qd;
	
    CAVITY:   RFCAVITY, L=0.1, VOLT={CC_MV},LAG={CC_LAG},HARMON={CC_H};

	index=0.199;
	
	
	EB: MATRIX, L =l.Ebend, 
	RM11 = 0.85418, RM12 = 3.30871, RM13 = 0, RM14 =0, RM15 =0, RM16 = 1.29205,

	RM21 = -0.0817166, RM22 = 0.85418, RM23 = 0, RM24 =0, RM25 =0, RM26 = 0.724056,

	RM31 = 0 , RM32 = 0 , RM33 = 1., RM34  = 3.47954 ,RM35 =0, RM36 = 0,

	RM41 = 0 , RM42 = 0 , RM43 = 0., RM44  = 1. ,RM45 =0, RM46 = 0,

	RM51 = -0.724056, RM52 = -1.29205, RM53 = 0, RM54 =0, RM55 =1, RM56 = 2.94856,

	RM61 = 0, RM62 = 0 , RM63 = 0, RM64 =0, RM65 =0, RM66 = 1;
	
	onecell: Line=(d,SSQ,dq,BPM,ds,ds,dq,Qf,d,Qf,dq,BPM,dEbend,EB,EB,dEbend,BPM,dq,Qd,d,Qd,dq,BPM,dEbend,EB,EB,dEbend,BPM,dq,Qf,d,Qf,dq,ds,ds,dq,SSQ,d);
	ProEDM: Line=(4*onecell + CAVITY);
	
	beam, PARTICLE=PROTON,MASS=pmass,npart = {bunch_intensity}, CHARGE=1,ENERGY={energy_GEV},EXN={emit_x},EYN={emit_y},bunched=True;
	use, sequence=ProEDM;
	
	select, flag=twiss, clear;
	select, flag=twiss, column= NAME,S,BETX,ALFX,MUX,X,PX,DX,DPX,BETY,ALFY,MUY,Y,PY,DY,DPY ;
	twiss,save,file="PTR-33M.tfs";
	
	
	!plot,table=twiss,haxis=s,hmin=0,hmax=110,vaxis1=BETX,BETY,DX,colour=100,Range=#S/#E,Noversion=True,noline=True,title="PTR Lattice", file="lattice";
	
   save,sequence=ProEDM,file="ProEDM.seq";

''')

madx.input('''
show, beam;
gt=table(summ,gammatr);
show, gt;
value, beam->gamma;
''')
n_slice_per_element = 4

twthick = madx.twiss().dframe()
#madx.input(f'''
#select, flag=MAKETHIN, SLICE={n_slice_per_element}, thick=false;
#MAKETHIN, SEQUENCE=ProEDM, MAKEDIPEDGE=false;
#use, sequence=ProEDM;''')
#twthin = madx.twiss().dframe()
#line = xt.Line.from_madx_sequence(madx.sequence['ProEDM'])


p0 = xp.Particles(mass0=xp.PROTON_MASS_EV, q0=1, p0c=energy_GEV*1e9*betarel(energy_GEV))
#tracker = xt.Tracker(line=line)
#tw = tracker.twiss(particle_ref = p0)

gtrans          = madx.table["summ"].dframe()["gammatr"].values[0]
my_eta = eta(gtrans, energy_GEV)

IBS = NagaitsevIBS()
IBS.set_beam_parameters(p0)
IBS.Npart = bunch_intensity
if not np.isclose(energy_GEV, IBS.EnTot):
    IBS.EnTot=energy_GEV
    IBS.gammar = gammarel(energy_GEV)
    IBS.betar = betarel(energy_GEV)

#IBS.set_optic_functions(tw)
IBS.set_optic_functions(twthick)
IBS.slip = my_eta
#print(tw)
#quit()

emit_x_all   = []
emit_y_all   = []
sigma_z_all  = []
txh          = []
tyh          = []
tlh          = []
time         = []

emit_x_all.append(emit_x)
emit_y_all.append(emit_y)
sigma_z_all.append(sigma_z) 
txh.append(0.0)
tyh.append(0.0)
tlh.append(0.0)
time.append(0.0)

for time_step in range(int(duration/dt)):
  emit_x         = emit_x_all[-1]/(IBS.betar*IBS.gammar)
  emit_y         = emit_y_all[-1]/(IBS.betar*IBS.gammar)
  
  Sigma_E = EnergySpread(C, CC_H, IBS.EnTot*1e9, IBS.slip, sigma_z_all[-1], IBS.betar, CC_MV*1e6, 0, IBS.Ncharg)
  Sig_M = Sigma_E/IBS.betar**2
  IBS.calculate_integrals(emit_x,emit_y,Sig_M,sigma_z_all[-1])
  Emit_x, Emit_y, Sig_M = IBS.emit_evol(emit_x,emit_y,Sig_M,sigma_z_all[-1], dt)
  #Emit_x, Emit_y, Sig_M = IBS.emit_evol_with_SR(emit_x,emit_y,Sig_M,sigma_z_all[-1], EQemitX, EQemitY, EQsigmM, tau_x, tau_y, tau_s,dt)
  Sigma_E = Sig_M*IBS.betar**2
  BunchL = BunchLength(IBS.Circu, CC_H, IBS.EnTot, IBS.slip,
                    Sigma_E, IBS.betar, CC_MV*1e-3, 0.0, IBS.Ncharg)

  print(Emit_x*IBS.betar*IBS.gammar, Emit_y*IBS.betar*IBS.gammar, BunchL, time_step)
  emit_x_all.append(Emit_x*IBS.betar*IBS.gammar)
  emit_y_all.append(Emit_y*IBS.betar*IBS.gammar)
  sigma_z_all.append(BunchL)
  time.append( (time_step+1)*dt)
  txh.append(1.0/float(IBS.Ixx)/3600.0)
  tyh.append(1.0/float(IBS.Iyy)/3600.0)
  tlh.append(1.0/float(IBS.Ipp)/3600.0)
df = pd.DataFrame({'en_x': emit_x_all, 'en_y':emit_y_all, 'sigma_z': sigma_z_all, 'txh': txh, 'tyh': tyh, 'tlh':tlh, 'time':time, 'bunch_intensity': bunch_intensity, 'sigma_ns':sigma_ns, 'emit_x0':emit_x,'emit_y0':emit_y})



